// ---
// toggle functions
// hamburger menu
function toggleMenu() {
  $("#js-navigation__menu").toggleClass("js-navigation__menu--display");
  $("body").toggleClass("js-overflow-hidden");

  $('.footer').toggleClass('js-footer--z-index');
  $('.cookies').toggleClass('js-cookies--z-index');
}

// high contrast
function toggleContrast() {
  $("#js-html").toggleClass("js-high-contrast");
}

// greater font
function toggleFontSize() {
  $("#js-html").toggleClass("js-greater-font");
  $("#js-marquee__span").toggleClass("js-marquee-padding--lower");
  $("#js-marquee__text__p").toggleClass("js-marquee-padding--lower");
}

// search button
function toggleSearchBar() {
  $("#js-search-bar").toggleClass("js-search-bar--display");
  $("#js-navigation__main__buttons__search__svg").toggleClass(
    "js-navigation__main__buttons__search__svg--opened"
  );
}
// ---

// ---
// close cookies info
function closeCookieInfo() {
  $("#js-cookies").addClass("cookies--hidden");
}
// ---

// ---
// document ready
$(document).ready(function () {
  // adding event listeners
  // hamburger menu
  $("#js-navigation__main__buttons__menu").on("click", function () {
    toggleMenu();
  });
  $("#js-navigation__toggle-menu").on("click", function () {
    toggleMenu();
  });

  // high contrast
  $("#js-navigation__main__buttons__contrast").on("click", function () {
    toggleContrast();
  });
  $("#js-navigation__menu__box__actions__contrast").on("click", function () {
    toggleContrast();
  });

  // greater font
  $("#js-navigation__main__buttons__font-size").on("click", function () {
    toggleFontSize();
  });
  $("#js-navigation__menu__box__actions__font-size").on("click", function () {
    toggleFontSize();
  });

  // search button
  $("#js-navigation__main__buttons__search").on("click", function () {
    toggleSearchBar();
  });

  // cookies
  $("#js-cookies__close-btn").on("click", function () {
    closeCookieInfo();
  });
});
// ---

// ---
// setting checkboxes to unchecked
var checkboxes = document.getElementsByTagName("input");

for (var i = 0; i < checkboxes.length; i++) {
  if (checkboxes[i].type == "checkbox") {
    checkboxes[i].checked = false;
  }
}
// ---

// ---
// pc sub nav
function addSubNavListeners(arg) {
  $(`#js-subpage-nav__menu__second__back-btn--${arg}`).on("click", function () {
    $(`#js-subpage-nav__menu__second__back-btn--${arg}`)
      .parent()
      .toggleClass("d-xl-block");
  });

  $(`#js-subpage-nav__menu__btn--${arg}`).on("click", function () {
    $(`#js-subpage-nav__menu__btn--${arg}`).next().toggleClass("d-xl-block");
  });
}

// adding listeners
// bip artykol
addSubNavListeners("urzad-miasta-i-gminy-kornik");
addSubNavListeners("struktura-organizacyjna");
addSubNavListeners("akty-prawne");
addSubNavListeners("burmistrz-miasta-i-gminy-kornik");
addSubNavListeners("jak-zalatwic-sprawe-w-urzedzie-miasta");
addSubNavListeners("jednostki-organizacyjne-samorzadu-terytorialnego");
addSubNavListeners("przetargi");
addSubNavListeners("rada-miasta-i-gminy-kornik");

// bip podstrona
addSubNavListeners("statystyki-dzialalnosci");

// open right submenu at start
$("#js-subpage-nav__menu__link--active")
  .parent()
  .parent()
  .addClass("d-xl-block");
// ---
