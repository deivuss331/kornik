function generateSlider() {
    $(window).width() < 768
        ? $("#js-insta__slider").slick({ infinite: !0, slidesToShow: 3, slidesToScroll: 3 })
        : $(window).width() >= 768 && $(window).width() < 1180
        ? $("#js-insta__slider").slick({ infinite: !0, slidesToShow: 4, slidesToScroll: 4 })
        : $(window).width() > 1180 && $("#js-insta__slider").slick({ infinite: !0, slidesToShow: 6, slidesToScroll: 6 });
}

var events = [
        { date: "2020-04-7", note: "Kórnicki Mikołaj na Rynku", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-04-7", dateTo: "2020-04-7" },
        { date: "2020-04-10", note: "Spotkanie w sprawie przebudowy świetlicy", type: "samorząd", place: "Kórnik", dateFrom: "2020-04-10", dateTo: "2020-04-10" },
        { date: "2020-04-12", note: "Koncert na Rynku", type: "samorząd", place: "Kórnik", dateFrom: "2020-04-12", dateTo: "2020-04-12" },
        { date: "2020-04-24", note: "Koncert w Zamku", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-04-24", dateTo: "2020-04-24" },
        { date: "2020-04-29", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2020-04-29", dateTo: "2020-04-29" },
        { date: "2020-05-11", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2020-05-11", dateTo: "2020-05-11" },
        { date: "2020-05-15", note: "Mecz siatkówki", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-05-15", dateTo: "2020-05-15" },
        { date: "2020-05-23", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2020-05-23", dateTo: "2020-05-23" },
        { date: "2020-06-7", note: "Kórnicki Mikołaj na Rynku", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-06-7", dateTo: "2020-06-7" },
        { date: "2020-06-10", note: "Spotkanie w sprawie przebudowy świetlicy", type: "samorząd", place: "Kórnik", dateFrom: "2020-06-10", dateTo: "2020-06-10" },
        { date: "2020-06-12", note: "Koncert na Rynku", type: "samorząd", place: "Kórnik", dateFrom: "2020-06-12", dateTo: "2020-06-12" },
        { date: "2020-06-24", note: "Koncert w Zamku", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-06-24", dateTo: "2020-06-24" },
        { date: "2020-06-29", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2020-06-29", dateTo: "2020-06-29" },
        { date: "2020-07-11", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2020-07-11", dateTo: "2020-07-11" },
        { date: "2020-07-15", note: "Mecz siatkówki", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-07-15", dateTo: "2020-07-15" },
        { date: "2020-07-23", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2020-07-23", dateTo: "2020-07-23" },
        { date: "2020-08-7", note: "Kórnicki Mikołaj na Rynku", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-08-7", dateTo: "2020-08-7" },
        { date: "2020-08-10", note: "Spotkanie w sprawie przebudowy świetlicy", type: "samorząd", place: "Kórnik", dateFrom: "2020-08-10", dateTo: "2020-08-10" },
        { date: "2020-08-12", note: "Koncert na Rynku", type: "samorząd", place: "Kórnik", dateFrom: "2020-08-12", dateTo: "2020-08-12" },
        { date: "2020-08-24", note: "Koncert w Zamku", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-08-24", dateTo: "2020-08-24" },
        { date: "2020-08-29", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2020-08-29", dateTo: "2020-08-29" },
        { date: "2020-09-11", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2020-09-11", dateTo: "2020-09-11" },
        { date: "2020-09-15", note: "Mecz siatkówki", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-09-15", dateTo: "2020-09-15" },
        { date: "2020-09-23", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2020-09-23", dateTo: "2020-09-23" },
        { date: "2020-10-7", note: "Kórnicki Mikołaj na Rynku", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-10-7", dateTo: "2020-10-7" },
        { date: "2020-10-10", note: "Spotkanie w sprawie przebudowy świetlicy", type: "samorząd", place: "Kórnik", dateFrom: "2020-10-10", dateTo: "2020-10-10" },
        { date: "2020-10-12", note: "Koncert na Rynku", type: "samorząd", place: "Kórnik", dateFrom: "2020-10-12", dateTo: "2020-10-12" },
        { date: "2020-10-24", note: "Koncert w Zamku", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-10-24", dateTo: "2020-10-24" },
        { date: "2020-10-29", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2020-10-29", dateTo: "2020-10-29" },
        { date: "2020-11-11", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2020-11-11", dateTo: "2020-11-11" },
        { date: "2020-11-15", note: "Mecz siatkówki", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-11-15", dateTo: "2020-11-15" },
        { date: "2020-11-23", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2020-11-23", dateTo: "2020-11-23" },
        { date: "2020-12-7", note: "Kórnicki Mikołaj na Rynku", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-12-7", dateTo: "2020-12-7" },
        { date: "2020-12-10", note: "Spotkanie w sprawie przebudowy świetlicy", type: "samorząd", place: "Kórnik", dateFrom: "2020-12-10", dateTo: "2020-12-10" },
        { date: "2020-12-12", note: "Koncert na Rynku", type: "samorząd", place: "Kórnik", dateFrom: "2020-12-12", dateTo: "2020-12-12" },
        { date: "2020-12-24", note: "Koncert w Zamku", type: "wydarzenie", place: "Kórnik", dateFrom: "2020-12-24", dateTo: "2020-12-24" },
        { date: "2020-12-29", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2020-12-29", dateTo: "2020-12-29" },
        { date: "2021-01-11", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2021-01-11", dateTo: "2021-01-11" },
        { date: "2021-01-15", note: "Mecz siatkówki", type: "wydarzenie", place: "Kórnik", dateFrom: "2021-01-15", dateTo: "2021-01-15" },
        { date: "2021-01-23", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2021-01-23", dateTo: "2021-01-23" },
        { date: "2021-02-7", note: "Kórnicki Mikołaj na Rynku", type: "wydarzenie", place: "Kórnik", dateFrom: "2021-02-7", dateTo: "2021-02-7" },
        { date: "2021-02-10", note: "Spotkanie w sprawie przebudowy świetlicy", type: "samorząd", place: "Kórnik", dateFrom: "2021-02-10", dateTo: "2021-02-10" },
        { date: "2021-02-12", note: "Koncert na Rynku", type: "samorząd", place: "Kórnik", dateFrom: "2021-02-12", dateTo: "2021-02-12" },
        { date: "2021-02-24", note: "Koncert w Zamku", type: "wydarzenie", place: "Kórnik", dateFrom: "2021-02-24", dateTo: "2021-02-24" },
        { date: "2021-02-29", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2021-02-29", dateTo: "2021-02-29" },
        { date: "2021-03-11", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2021-03-11", dateTo: "2021-03-11" },
        { date: "2021-03-15", note: "Mecz siatkówki", type: "wydarzenie", place: "Kórnik", dateFrom: "2021-03-15", dateTo: "2021-03-15" },
        { date: "2021-03-23", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2021-03-23", dateTo: "2021-03-23" },
        { date: "2021-04-7", note: "Kórnicki Mikołaj na Rynku", type: "wydarzenie", place: "Kórnik", dateFrom: "2021-04-7", dateTo: "2021-04-7" },
        { date: "2021-04-10", note: "Spotkanie w sprawie przebudowy świetlicy", type: "samorząd", place: "Kórnik", dateFrom: "2021-04-10", dateTo: "2021-04-10" },
        { date: "2021-04-12", note: "Koncert na Rynku", type: "samorząd", place: "Kórnik", dateFrom: "2021-04-12", dateTo: "2021-04-12" },
        { date: "2021-04-24", note: "Koncert w Zamku", type: "wydarzenie", place: "Kórnik", dateFrom: "2021-04-24", dateTo: "2021-04-24" },
        { date: "2021-04-29", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2021-04-29", dateTo: "2021-04-29" },
        { date: "2021-05-11", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2021-05-11", dateTo: "2021-05-11" },
        { date: "2021-05-15", note: "Mecz siatkówki", type: "wydarzenie", place: "Kórnik", dateFrom: "2021-05-15", dateTo: "2021-05-15" },
        { date: "2021-05-23", note: "Sesja rady miejskiej", type: "samorząd", place: "Kórnik", dateFrom: "2021-05-23", dateTo: "2021-05-23" },
];

var eventCalendar = $("#js-calendar__box__widget").dnCalendar({
    dataTitles: { defaultDate: "default", today: "Today" },
    notes: events,
    showNotes: !0,
    monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
    monthNamesShort: ["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"],
    dayNames: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"],
    dayNamesShort: ["Nd", "Pn", "Wt", "Śr", "Cz", "Pt", "Sb"],
    dayUseShortName: !0,
    startWeek: "monday",
    dataTitles: { defaultDate: "default", today: "" },
});
if ((eventCalendar.build(), $("#js-calendar__box__widget--second").length > 0)) {
    var secondEventCalendar = $("#js-calendar__box__widget--second").dnCalendar({
        dataTitles: { defaultDate: "default", today: "Today" },
        notes: events,
        showNotes: !1,
        monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
        monthNamesShort: ["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"],
        dayNames: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"],
        dayNamesShort: ["Nd", "Pn", "Wt", "Śr", "Cz", "Pt", "Sb"],
        dayUseShortName: !0,
        startWeek: "monday",
        dataTitles: { defaultDate: "default", today: "" },
    });
    secondEventCalendar.build();
}
if ($("#js-calendar__box__widget--third").length > 0) {
    var thirdEventCalendar = $("#js-calendar__box__widget--third").dnCalendar({
        dataTitles: { defaultDate: "default", today: "Today" },
        notes: events,
        showNotes: !1,
        monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
        monthNamesShort: ["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"],
        dayNames: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"],
        dayNamesShort: ["Nd", "Pn", "Wt", "Śr", "Cz", "Pt", "Sb"],
        dayUseShortName: !0,
        startWeek: "monday",
        dataTitles: { defaultDate: "default", today: "" },
    });
    thirdEventCalendar.build();
}
function updateCalendarPadding() {
    $("#js-calendar__box").css("padding-bottom", "0"),
        $("#js-pos-xl-rel").css("margin-bottom", "0"),
        window.innerWidth < 1180
            ? $("#calendar__box__events").length > 0
                ? $("#js-calendar__box").css("padding-bottom", $("#calendar__box__events").height() + 100 + "px")
                : $("#js-calendar__box").css("padding-bottom", "60px")
            : $("#calendar__box__events").height() > 415
            ? $("#js-calendar__box").css("padding-bottom", $("#calendar__box__events").height() - 415 + "px")
            : $("#js-calendar__box").css("padding-bottom", "40px");
}
function toggleMenu() {
    $("#js-navigation__menu").toggleClass("js-navigation__menu--display"), $("body").toggleClass("js-overflow-hidden");
}
function toggleContrast() {
    $("#js-html").toggleClass("js-high-contrast");
}
function toggleFontSize() {
    $("#js-html").toggleClass("js-greater-font"), $("#js-marquee__span").toggleClass("js-marquee-padding--lower"), $("#js-marquee__text__p").toggleClass("js-marquee-padding--lower");
}
function toggleSearchBar() {
    $("#js-search-bar").toggleClass("js-search-bar--display"), $("#js-navigation__main__buttons__search__svg").toggleClass("js-navigation__main__buttons__search__svg--opened");
}
function closeCookieInfo() {
    $("#js-cookies").addClass("cookies--hidden");
}
function openSubmenu(e) {
    var t = [];
    switch (e) {
        case "gmina-kornik":
            t = ["sprawy-obywatelskie", "aktualnosci", "strony-tematyczne", "kontakt"];
            break;
        case "sprawy-obywatelskie":
            t = ["gmina-kornik", "aktualnosci", "strony-tematyczne", "kontakt"];
            break;
        case "aktualnosci":
            t = ["gmina-kornik", "sprawy-obywatelskie", "strony-tematyczne", "kontakt"];
            break;
        case "strony-tematyczne":
            t = ["gmina-kornik", "sprawy-obywatelskie", "aktualnosci", "kontakt"];
            break;
        case "kontakt":
            t = ["gmina-kornik", "sprawy-obywatelskie", "aktualnosci", "strony-tematyczne"];
            break;
        default:
            return;
    }
    t.forEach(function (e) {
        $(`#js-pc-submenu__${e}`).removeClass("js-submenu--display"), $(`#js-nav-${e}`).removeClass("navigation__menu__box__first-col__label--active");
    }),
        $(`#js-pc-submenu__${e}`).toggleClass("js-submenu--display"),
        $(`#js-nav-${e}`).toggleClass("navigation__menu__box__first-col__label--active");
}
function toggleEventCheckbox(e) {
    $(`#js-event-list__checkbox__flex__parent__input--custom__${e}`).length > 0 &&
        $(`#js-event-list__checkbox__flex__parent__input--${e}`).on("change", function () {
            $(`#js-event-list__checkbox__flex__parent__input--custom__${e}`).toggleClass(`js-event-list__checkbox__flex__parent__input--custom__${e}`),
                $(`#js-event-list__checkbox__flex__parent__input--${e}`).is(":checked")
                    ? "samorzad" === e
                        ? updateEventList("#samorząd")
                        : "wydarzenia" === e && updateEventList("#wydarzenie")
                    : "samorzad" === e
                    ? deleteEvents("#samorząd")
                    : "wydarzenia" === e && deleteEvents("#wydarzenie");
        }),
        $(`#js-calendar__box__checkbox__checkboxes__flex__parent__input--custom__${e}`).length > 0 &&
            $(`#js-calendar__box__checkbox__checkboxes__flex__parent__input--${e}`).on("change", function () {
                $(`#js-calendar__box__checkbox__checkboxes__flex__parent__input--custom__${e}`).toggleClass(`js-event-list__checkbox__flex__parent__input--custom__${e}`),
                    $(`#js-calendar__box__checkbox__checkboxes__flex__parent__input--${e}`).is(":checked")
                        ? "samorzad" === e
                            ? updateEventList("#samorząd")
                            : "wydarzenia" === e && updateEventList("#wydarzenie")
                        : "samorzad" === e
                        ? deleteEvents("#samorząd")
                        : "wydarzenia" === e && deleteEvents("#wydarzenie");
            });
}
function updateHeaderContent() {
    $("#js-event-list__checkbox__h2").length > 0 && $("#js-event-list__checkbox__h2").text($("#js-calendar--month").text());
}
$("#js-nav-gmina-kornik").click(function () {
    openSubmenu("gmina-kornik");
}),
    $("#js-nav-sprawy-obywatelskie").click(function () {
        openSubmenu("sprawy-obywatelskie");
    }),
    $("#js-nav-aktualnosci").click(function () {
        openSubmenu("aktualnosci");
    }),
    $("#js-nav-strony-tematyczne").click(function () {
        openSubmenu("strony-tematyczne");
    }),
    $("#js-nav-kontakt").click(function () {
        openSubmenu("kontakt");
    });
var events = [];
function getThisMonthEvents() {
    (events = []),
        $(".calendar__box__events__event-box--single-event").each(function () {
            events.push(this);
        });
}
function clearMonthEvents() {
    deleteEvents("#wydarzenie"),
        deleteEvents("#samorząd"),
        $("#js-event-list__checkbox__flex__parent__input--custom__samorzad").removeClass("js-event-list__checkbox__flex__parent__input--custom__samorzad"),
        $("#js-event-list__checkbox__flex__parent__input--custom__wydarzenia").removeClass("js-event-list__checkbox__flex__parent__input--custom__wydarzenia"),
        $("#js-event-list__checkbox__flex__parent__input--samorzad").prop("checked", !1),
        $("#js-event-list__checkbox__flex__parent__input--wydarzenia").prop("checked", !1),
        $("#js-calendar__box__checkbox__checkboxes__flex__parent__input--custom__samorzad").removeClass("js-event-list__checkbox__flex__parent__input--custom__samorzad"),
        $("#js-calendar__box__checkbox__checkboxes__flex__parent__input--custom__wydarzenia").removeClass("js-event-list__checkbox__flex__parent__input--custom__wydarzenia"),
        $("#js-calendar__box__checkbox__checkboxes__flex__parent__input--samorzad").prop("checked", !1),
        $("#js-calendar__box__checkbox__checkboxes__flex__parent__input--wydarzenia").prop("checked", !1);
}
function updateEventList(e) {
    $("#event-list__checkbox__events-parent__events").append(
        events.filter(function (t) {
            if (t.children[1].children[1].textContent === e) return t;
        })
    );
}
function deleteEvents(e) {
    events
        .filter(function (t) {
            if (t.children[1].children[1].textContent === e) return t;
        })
        .forEach(function (e) {
            e.remove();
        });
}
function addEventCheckboxes(e) {
    $(`#add-event__${e}`).length > 0 &&
        $(`#add-event__${e}`).on("change", function () {
            $(`#add-event__${e}`).next().toggleClass("add-event-form__section__checkbox-parent__icon--checked");
        });
}
getThisMonthEvents(),
    $("#add-event__desc").length > 0 &&
        $("#add-event__desc").summernote({
            toolbar: [
                ["font", ["bold"]],
                ["para", ["ul", "ol"]],
            ],
        }),
    addEventCheckboxes("terms"),
    addEventCheckboxes("cookies"),
    $(document).ready(function () {
        $(".js-slider__box").slick({ dots: !0, autoplay: !0 }),
            generateSlider(),
            updateCalendarPadding(),
            $(window).resize(function () {
                updateCalendarPadding();
            }),
            $("#js-navigation__main__buttons__menu").on("click", function () {
                toggleMenu();
            }),
            $("#js-navigation__toggle-menu").on("click", function () {
                toggleMenu();
            }),
            $("#js-navigation__main__buttons__contrast").on("click", function () {
                toggleContrast();
            }),
            $("#js-navigation__menu__box__actions__contrast").on("click", function () {
                toggleContrast();
            }),
            $("#js-navigation__main__buttons__font-size").on("click", function () {
                toggleFontSize();
            }),
            $("#js-navigation__menu__box__actions__font-size").on("click", function () {
                toggleFontSize();
            }),
            $("#js-navigation__main__buttons__search").on("click", function () {
                toggleSearchBar();
            }),
            $("#js-cookies__close-btn").on("click", function () {
                closeCookieInfo();
            }),
            toggleEventCheckbox("wydarzenia"),
            toggleEventCheckbox("samorzad"),
            updateHeaderContent();
    });
for (var checkboxes = document.getElementsByTagName("input"), i = 0; i < checkboxes.length; i++) "checkbox" == checkboxes[i].type && (checkboxes[i].checked = !1);
